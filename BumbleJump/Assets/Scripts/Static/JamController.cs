﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
public class JamController : MonoBehaviour
{
        public AudioSource source;

    // Start is called before the first frame update
     void OnTriggerEnter(Collider other)
    {
        //Debug.Log(gameObject.tag);
        var playerScript = other.gameObject.GetComponent<PlayerController>();
        if(other.gameObject.tag == "Player")
        {
            source.Play();
            playerScript.winText.text = "Press A to return to main menu Percentage completed: " + (playerScript.jamCount / 26f * 100).ToString("f2") + "%";
            playerScript.Win();
        }
        Debug.Log(playerScript.jamCount);
        //other.gameObject.SetActive(false);   
    }
}
