﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathfExtra : object
{
    public static float Vector2ToDegrees(Vector2 direction)
    {
        float angle = Mathf.Acos(direction.normalized.x) * Mathf.Rad2Deg;
        if (direction.normalized.y < 0)
        {
            angle = 360 - angle;
        }
        angle -= 90;
        return angle;
    }

    public static Vector2 DegreesToVector2(float degrees)
    {
        Vector2 direction;
        direction = new Vector2(Mathf.Cos(degrees * Mathf.Deg2Rad), Mathf.Sin(degrees * Mathf.Deg2Rad));
        return direction;
    }

    public static float CalculateInitialVelocity(float distance, float deltaVelocity)
    {
        float power = 0;
        power = Mathf.Sqrt(Mathf.Abs(2 * distance * deltaVelocity)) * PosNegZero(distance) * PosNegZero(deltaVelocity);
        return power;
    }

    public static float CalculateDeceleration(float distance, float initialSpeed)
    {
        float deceleration = 0;
        deceleration = initialSpeed * initialSpeed / (2 * distance);
        return deceleration;
    }

    public static int PosNegZero(float value)
    {
        if (value > 0)
            value = 1;
        else if (value < 0)
            value = -1;
        return (int)value;
    }

    public static float ReduceAngle(float angle)
    {
        while (angle >= 360)
        {
            angle -= 360;
        }
        while (angle <= -360)
        {
            angle += 360;
        }
        return angle;
    }
}
