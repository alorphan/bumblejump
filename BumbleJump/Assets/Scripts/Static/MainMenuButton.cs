﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuButton : MonoBehaviour
{
    
    public int bufferDuration; // In frames
    [HideInInspector] public Button A;
    // Start is called before the first frame update

    // Update is called once per frame
    void FixedUpdate()
    {
         A = GetButton(A, "AButton");

         if(A.held == 1)
         {
           SceneManager.LoadScene("TheHiveScene");
         }
    }

    private Button GetButton(Button button, string inputString)
    {
        // If the button was pressed then increase the button held counter else set it back to 0.
        if (Input.GetButton(inputString))
            button.held++;
        else
            button.held = 0;
        // Decrease the button buffer if it is active.
        if (button.buffer > 0)
            button.buffer--;
        // If the button was pressed then set the buffer to 10.
        if (button.held == 1)
            button.buffer = bufferDuration; // 10 is the input buffer.

        return button;
    }
}
