﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player; // This is a public transform reference to what the camera will follow.
    public GameObject fadeObject;
    public GameObject timer;
    public float maxDistance; // The maxium distance the camera will be from it's target.
    [Range(0, 1)] public float followStrength; // Interpolate strength the camera will follow.
    [Range(0, 1)] public float turnStrength; // Interpolate strength the camera will turn on the x-axis (actually the y-axis).
    public float controlSpeed; // The speed that the player can control the camera.

    private Vector3 targetAngle; // The angle that the camera will move towards.
    private Vector3 targetPosition; // The position that the camera will move towards.

    private PlayerInput pi; // The reference to the player input component in the player gameobject.
    [HideInInspector] public FadeScript fs;
    [HideInInspector] public TimerScript ts;

    void Start()
    {
        targetAngle = transform.eulerAngles; // Set the target angle to it's angle in the unity editor.
        pi = player.gameObject.GetComponent<PlayerInput>();
        fs = fadeObject.GetComponent<FadeScript>();
        ts = timer.GetComponent<TimerScript>();
        fs.FadeIn(0.5f, 0.25f, 0);
    }

    void FixedUpdate()
    {
        /*** Calculate the rotation of the camera ***/

        // Set the new angle to it's current angle. We can't change the axes of transform.eulerAngles indiviually so we have to use this.
        Vector3 newAngle = transform.eulerAngles;
        //targetAngle = transform.eulerAngles;

        // Change the target angle based on the right stick horizontal input and 'controlSpeed'
        targetAngle.x += pi.RS.direction.y * Time.deltaTime * controlSpeed;
        targetAngle.y += pi.RS.direction.x * Time.deltaTime * controlSpeed;

        /* Interpolate the current angle to the target angle using turnStrength
         (I'll do it one axis at a time otherwise the camera freaks out and so we can use LerpAngle).
         Using interpolation wil give the camera a smooth feeling! */
        newAngle.x = Mathf.LerpAngle(newAngle.x, targetAngle.x, turnStrength);
        newAngle.y = Mathf.LerpAngle(newAngle.y, targetAngle.y, turnStrength);

        // Clamp the angle for the x axis (but actually its the y axis)
        newAngle.x = Mathf.Clamp(newAngle.x, 0, 85);
        targetAngle.x = Mathf.Clamp(targetAngle.x, 0, 85);

        // Set the transform angle to our new, smooth-looking calulated angle.
        transform.eulerAngles = newAngle;

        /*** Calculate the position of the camera ***/

        // Set the targetPosition to the players's current position
        targetPosition = player.position;

        // Get the direction that the camera is facing.
        Vector3 rotationDirection = Quaternion.Euler(targetAngle) * Vector3.back;

        // Add the player's position, direction of the camera (multiplied by the max distance), and a (0, 1, 0) for good measure.
        targetPosition = player.position + rotationDirection * maxDistance + Vector3.up;

        // Interpolate the current position to the target angle using followStrength.
        transform.position = Vector3.Lerp(transform.position, targetPosition, followStrength);

        // *** Avoiding wall clipping *** //
        // Get the ground layer value.
        int groundMask = 1 << 9;
        // Create a Raycast object.
        RaycastHit wallRay = new RaycastHit();
        // The actual cast.
        //Vector3 flattenCamera = rotationDirection;
        //flattenCamera.y = player.position.y;
        bool wallRayHit = Physics.Raycast(player.position, rotationDirection, out wallRay, maxDistance, groundMask);
        // Create a variable for distance.
        float camDistance = maxDistance;

        // If the wall ray hits then re-position the camera.
        if (wallRayHit)
            camDistance = wallRay.distance;
        // Instantly snap the position to the desired distance.
        transform.position = player.position + Quaternion.Euler(newAngle) * Vector3.back * camDistance + Vector3.up;    
    }
}

