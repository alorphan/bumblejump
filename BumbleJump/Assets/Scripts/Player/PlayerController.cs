﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private int collectedJam; 
    public Text collectedJamText; 
    public Text winText; 
    [Header("References")]
    public Transform mainCamera; // A public transform reference to the main camera.
    [Header("Movement")]
    public float moveSpeed;
    public float moveAcceleration; // The acceleration at which the player will move.
    public float moveDeceleration;
    public float jumpHeight; // The jump height.
    [Header("Gliding")]
    public float glideDelay;
    public float initalGlideDuration;
    public float glideStrength;
    public float initalGlideStrength;
    public float glideKick;
    public float glideTorqueStrength;

    [Header("Abilities")] 
    public bool canJump;
    public bool canGlide;
    // Boolean
    public int jamCount; 
    private bool isGrounded; // Can you jump or not?
    //private bool stableGround; // This is for the Collision object.
    [HideInInspector] public bool isDead;
    private bool winner;
    private int deathDuration;
    private Vector3 initialPosition;
    public AudioSource deathSound;
    public AudioSource jumpSound;

    // Components: These are the references to the components of this object.
    private Rigidbody rb; // A private reference to this gameObjects Rigidbody component. We set the reference in Start().
    private PlayerInput pi; // The the player input class, we get all of the inputs from here.
    private Animator anim;
    private CameraController cc;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Search this gameObject for a Rigidbody component and assign it to rb.
        pi = GetComponent<PlayerInput>(); // Search this gameObject for a PlayerInput compoent and assign it to pi.
        anim = GetComponent<Animator>();
      
        winText.text = ""; 
        collectedJam = 0; 
        cc = mainCamera.gameObject.GetComponent<CameraController>();

        initialPosition = transform.position;
    }

    void FixedUpdate()
    {
        // *** Ground Detection (Sphere Casting) *** //
        int groundMask = 1 << 8;
        RaycastHit groundRay = new RaycastHit();
        bool cast = Physics.SphereCast(transform.position, transform.localScale.x * 0.9f, Vector3.down, out groundRay, 2, groundMask);

        //if (!cast)
        //    stableGround = false;
        //if (stableGround && cast)
        //    isGrounded = true;
        //if (!cast && !stableGround)
        //    isGrounded = false;

        // *** Rolling and Camera Movement *** //

        // Get the left joystick inputs and put them in Vector2.
        //float horizontal = Input.GetAxisRaw("LSHorizontal");
        //float vertical = Input.GetAxisRaw("LSVertical");
        Vector2 leftStickInputs = new Vector2(pi.LS.direction.x, pi.LS.direction.y);

        // Note: MathfExtra is a stactic class that I wrote. It has a bunch of math related functions that Mathf doesn't have.
        // Skew the inputs based on the perspective of the camera. 
        float inputAngle = MathfExtra.Vector2ToDegrees(leftStickInputs); // Converts to degrees.
        inputAngle -= mainCamera.eulerAngles.y - 90; // Adds the camera angle.
        leftStickInputs = MathfExtra.DegreesToVector2(inputAngle) * leftStickInputs.magnitude; // Converts back to Vector2.

        // Handle movement.
        Move(leftStickInputs, moveAcceleration, moveDeceleration, moveSpeed);

        // *** Jump *** //

        // Check if the jump button has a buffer.
        if (pi.A.buffer > 0 && isGrounded && canJump == true)
        {
            // reset the buffer
            pi.A.buffer = 0;
            // Add the force
            rb.AddForce(Vector3.up * MathfExtra.CalculateInitialVelocity(jumpHeight, -Physics.gravity.y) * (1 / Time.deltaTime)); // -10 is the acceleration of gravity.
            jumpSound.Play();
        }
        isGrounded = false;

        // *** Glide *** //
        if (!isGrounded && canGlide)
        {
            if (pi.A.held > glideDelay + initalGlideDuration)
            {
                rb.AddForce(Vector3.up * glideStrength);
            }
            else if (pi.A.held > glideDelay)
            {            
                rb.AddForce(Vector3.up * initalGlideStrength);
            }
            else if (pi.A.held == glideDelay)
            {
                if (rb.velocity.y >= 0)
                {
                    rb.AddForce(Vector3.up * glideKick);
                    rb.velocity = new Vector3(rb.velocity.x, Mathf.Lerp(rb.velocity.y, 0, 0.75f), rb.velocity.z);
                }
            }

            if (pi.A.held >= glideDelay)
            {
                Vector3 torqueDirection = -transform.eulerAngles;
                rb.AddTorque(torqueDirection * glideTorqueStrength);
            }

            anim.SetBool("flapping", pi.A.held >= glideDelay && isGrounded);
        }
        SetCountText(); 
        //Debug.Log(rb.velocity);

        if (transform.position.y <= 0 && !isDead && !winner)
        {
            Death();
        }

        if (winner && pi.A.held == 1)
        {
            SceneManager.LoadScene("MainMenu");
        }

        if (isDead)
        {
            deathDuration++;
            if (deathDuration == 60)
            {
                cc.fs.FadeIn(0.5f, 0.25f, 0);
                transform.position = initialPosition;
                rb.velocity = Vector3.zero;
                isDead = false;
            }
        }

        //Debug.Log(rb.velocity);
    }

    private void Move(Vector2 input, float acceleration, float deceleration, float maxSpeed)
    {
        float storedYVelocity = rb.velocity.y;
        rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);

        // Add the force to the rigidbody using the acceleration variable.
        rb.AddForce(new Vector3(input.x, 0, input.y) * acceleration);
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = Vector3.MoveTowards(rb.velocity, rb.velocity.normalized * maxSpeed, acceleration * 1.05f * Time.deltaTime);
        }
        rb.velocity = Vector3.MoveTowards(rb.velocity, Vector3.zero, deceleration * Time.deltaTime);
        rb.velocity += Vector3.up * storedYVelocity;
    }

    private void OnCollisionStay(Collision collision)
    {
        // Get the contacts of the collsion.
        ContactPoint[] contacts = collision.contacts;
        // Check each of the contacts to see if any of the contacts 
        for (int i = 0; i < contacts.Length; i++)
        {
            // Get the angle of the normal.
            float angle = Vector3.Angle(Vector3.up, contacts[i].normal);
            // If the normal of the less than 45 degrees.
            if (Mathf.MoveTowards(angle, 0, 45f) == 0)
            {
                // The ground is stable and we should be allowed to jump off of it.
                isGrounded = true;
                // Exit the loop.
                i = contacts.Length;
            }
        }
    }

    void SetCountText() 
    {
        collectedJamText.text = "Jam Drops: " + jamCount.ToString(); 
    }

    public void Death()
    {
        if (!winner)
        {
            deathSound.Play();
            isDead = true;
            deathDuration = 0;
            cc.fs.FadeOut(0.25f, 0.5f, 1);
        }
    }
    

    public void Win()
    {
        winner = true;
        cc.fs.FadeOut(0.25f, 0.25f, 0.5f);
        cc.ts.SetPause(true);
    }
}

