﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct Button
{
    public int held;
    public int buffer;
}

public struct Stick
{
    public Vector2 direction;
}

public struct Trigger
{
    public float magnitude;
}

public class PlayerInput : MonoBehaviour
{
    public int bufferDuration; // In frames

    /* Buttons */
    [HideInInspector] public Button A;
    [HideInInspector] public Button B;
    [HideInInspector] public Button start;
    [HideInInspector] public Button back;

    /* Sticks */
    [HideInInspector] public Stick LS;
    [HideInInspector] public Stick RS;

    void Start()
    {

    }

    void FixedUpdate()
    {
        // Get the values for the left stick horizontal and vertical.
        LS.direction.x = Input.GetAxis("LSHorizontal");
        LS.direction.y = Input.GetAxis("LSVertical");
        // Get the values for the right stick horizontal and vertical.
        RS.direction.x = Input.GetAxis("RSHorizontal");
        RS.direction.y = Input.GetAxis("RSVertical");

        // Get the button input and change the held and buffer counters.
        A = GetButton(A, "AButton");
        B = GetButton(B, "BButton");
        start = GetButton(start, "StartButton");
        back = GetButton(back, "BackButton");

        if (start.held == 30)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        if (back.held == 1)
            Debug.Break();
    }

    private Button GetButton(Button button, string inputString)
    {
        // If the button was pressed then increase the button held counter else set it back to 0.
        if (Input.GetButton(inputString))
            button.held++;
        else
            button.held = 0;
        // Decrease the button buffer if it is active.
        if (button.buffer > 0)
            button.buffer--;
        // If the button was pressed then set the buffer to 10.
        if (button.held == 1)
            button.buffer = bufferDuration; // 10 is the input buffer.

        return button;
    }
}
