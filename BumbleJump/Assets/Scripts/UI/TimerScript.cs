﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    private bool paused;

    private Text textComp;
    private float totalTime;

    void Start()
    {
        textComp = GetComponent<Text>();
    }

    void FixedUpdate()
    {
        if (!paused)
        {
            totalTime += Time.deltaTime;

            string displayString = "";

            int min = (int)totalTime / 60;
            if (min >= 10)
                displayString += min + ":";
            else
                displayString += "0" + min + ":";

            int sec = (int)totalTime % 60;
            if (sec >= 10)
                displayString += sec + ":";
            else
                displayString += "0" + sec + ".";

            displayString += (int)((totalTime - (int)totalTime) * 10);

            textComp.text = displayString;
        }
    }

    public void SetPause(bool newPaused)
    {
        paused = newPaused;
    }
}
