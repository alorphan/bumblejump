﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JamMeterScript : MonoBehaviour
{
    public GameObject jamDropParent;
    public GameObject player;

    private int totalJamDrops;

    private Image img;
    private PlayerController pc;

    void Start()
    {
        totalJamDrops = jamDropParent.transform.childCount;
        img = GetComponent<Image>();
        pc = player.GetComponent<PlayerController>();
    }

    void FixedUpdate()
    {
        img.fillAmount = ((float)pc.jamCount / totalJamDrops) * 0.8f;
    }
}
