﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScript : MonoBehaviour
{
    private float fadeDelay;
    private float fadeTime;
    private float targetOpacity;

    private bool fadeIn;
    private float fadeOpacity;
    private float totalTime;

    private Color clr; // fade color

    private Image img; // fade sprite renderer

    void Start()
    {
        img = GetComponent<Image>();

        if (fadeIn)
            fadeOpacity = 1;
        else
            fadeOpacity = 0;

        clr = img.color;
        img.color = new Color(clr.r, clr.g, clr.b, fadeOpacity);
    }

    void FixedUpdate()
    {
        totalTime += Time.deltaTime;

        if (totalTime >= fadeDelay)
        {
            //if (fadeIn)
            //    fadeOpacity -= Time.deltaTime * 1 / fadeTime;
            //else
            //    fadeOpacity += Time.deltaTime * 1 / fadeTime;
            fadeOpacity = Mathf.MoveTowards(fadeOpacity, targetOpacity, Time.deltaTime * 1 / fadeTime);
            Debug.Log(fadeOpacity + ", " + targetOpacity);
        }

        fadeOpacity = Mathf.Clamp(fadeOpacity, 0, 1);
        img.color = new Color(clr.r, clr.g, clr.b, fadeOpacity);
    }

    public void FadeIn(float newDelay, float newTime, float newTargetOpacity)
    {
        Debug.Log("Fading in...");
        fadeIn = true;
        totalTime = 0;
        fadeOpacity = 1;
        fadeDelay = newDelay;
        fadeTime = newTime;
        targetOpacity = newTargetOpacity;
    }

    public void FadeOut(float newDelay, float newTime, float newTargetOpacity)
    {
        Debug.Log("Fading out...");
        fadeIn = false;
        totalTime = 0;
        fadeOpacity = 0;
        fadeDelay = newDelay;
        fadeTime = newTime;
        targetOpacity = newTargetOpacity;
    }
}
