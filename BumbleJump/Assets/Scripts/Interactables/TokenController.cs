﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TokenController : MonoBehaviour
{
    public string ability; 
    public Vector3 rotation;
    public AudioSource source;


    void Update() 
    {
        transform.Rotate(rotation * Time.deltaTime);
    }

       
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(gameObject.tag);
        var playerScript = other.gameObject.GetComponent<PlayerController>(); 
        if(ability == "JumpToken")
        {
            playerScript.canJump = true;
            source.Play();

        }
        else if (ability == "GlideToken")
        {
            playerScript.canGlide = true;
            source.Play();
        }
        else if(ability == "JamDrop")
        {
            //source.PlayOn
            playerScript.jamCount += 1;

            source.Play();
            
        }
        Debug.Log(playerScript.canJump);
        Debug.Log(playerScript.jamCount);
        Destroy(gameObject);
        //other.gameObject.SetActive(false);   
    }
}
