﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoneyController : MonoBehaviour
{
    public AudioSource source; 
      void OnTriggerEnter(Collider other)
    {
        var playerScript = other.gameObject.GetComponent<PlayerController>(); 

        //Debug.Log(gameObject.tag);
        if(other.tag == "Player")
        {
            playerScript.Death(); 
            source.Play();           
        }
    }
}
